//! Ethereum transaction state
//!

use alloc::collections::BTreeMap;
use evm::Context;
use primitive_types::{H160, H256, U256};

/// One transaction level
///
/// Calling an EVM contract initiates a new transaction unless it is a
/// delegate call. A transaction can update storage, code (contract
/// creation), and account data (balance, nonce).
pub struct Transaction {
    /// The context for the transaction - caller/callee, et.c.
    pub context: Context,
    /// Updates to storage
    pub updated_storage: BTreeMap<(H160, H256), H256>,
    /// Updates to account balance
    pub updated_balance: BTreeMap<H160, U256>,
    /// Updated code storage - holds new contracts not yet committed
    pub updated_code: BTreeMap<H160, Vec<u8>>,
}

#[allow(unused_variables)]
impl Transaction {
    /// Create a new transaction for a (possibyly sub-) context
    pub fn new(context: Context) -> Self {
        Self {
            context,
            updated_storage: BTreeMap::new(),
            updated_balance: BTreeMap::new(),
            updated_code: BTreeMap::new(),
        }
    }

    /// Set value in storage as part of current transaction. Transaction will
    /// need to be committed for this to take effect on durable storage.
    pub fn set_storage(&mut self, address: H160, index: H256, value: H256) {
        self.updated_storage.insert((address, index), value);
    }

    /// Get value from storage. Will return [Some] only if the value has been
    /// set as part of this transaction.
    pub fn get_storage(&self, address: H160, index: H256) -> Option<H256> {
        self.updated_storage.get(&(address, index)).cloned()
    }

    /// Get the balance of an account. Will only return [Some] if the balance
    /// of the address has been modified as part of this transaction.
    pub fn balance(&self, address: H160) -> Option<U256> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/115")
    }

    /// Get the code size of a contract. Will return [Some] only if the code
    /// has been created as port of this transaction.
    pub fn code_size(&self, address: H160) -> Option<U256> {
        self.updated_code.get(&address).map(|x| U256::from(x.len()))
    }

    /// Get the hash of the contract. Will return [Some] only if the code
    /// has been created as part of this transaction.
    pub fn code_hash(&self, address: H160) -> Option<H256> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/111")
    }

    /// Get the bytecode for a contract. Will return [Some] only if the code
    /// has been created as part of this transaction.
    pub fn code(&self, address: H160) -> Option<Vec<u8>> {
        // TODO do we really need to clone (or copy) here? User of this
        // function will never need to change the code.
        self.updated_code.get(&address).cloned()
    }
}
