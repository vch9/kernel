//! Deposit tickets into the kernel state.

/// All we use from the transactions kernel wrt deposits
pub mod tx {
    pub use kernel_core::deposit::deposit_ticket;
    pub use kernel_core::deposit::DepositError;
}
