//! Collection of unit kernels, testing usually a single host capability.
//!
//! These can all be built with no allocation required.

/// [`store_has`] test kernel.
///
/// This kernel expects a collection of values to exist:
/// - `/durable/hi/bye`
/// - `/durable/hello`
/// - `/durable/hello/universe
/// and asserts that `store_has` returns the correct type for each.
///
/// [`store_has`]: host::rollup_core::store_has
#[cfg(feature = "test-store-has")]
#[no_mangle]
pub unsafe extern "C" fn kernel_next() {
    use host::rollup_core::{RawRollupCore, ValueType};

    let mut host = host::wasm_host::WasmHost::new();
    let host = &mut host;

    let path = b"/hi";
    let result = RawRollupCore::store_has(host, path.as_ptr(), path.len());

    assert_eq!(result, ValueType::Subtree);

    let path = b"/hi/bye";
    let result = RawRollupCore::store_has(host, path.as_ptr(), path.len());

    assert_eq!(result, ValueType::Value);

    let path = b"/hello";
    let result = RawRollupCore::store_has(host, path.as_ptr(), path.len());

    assert_eq!(result, ValueType::ValueWithSubtree);

    let path = b"/hello/other";
    let result = RawRollupCore::store_has(host, path.as_ptr(), path.len());

    assert_eq!(result, ValueType::None);
}

/// [`write_debug`] test kernel.
///
/// This kernel checks that it can link to & call `write_debug`.
/// [`write_debug`]: host::rollup_core::write_debug
#[cfg(feature = "test-write-debug")]
#[no_mangle]
pub unsafe extern "C" fn kernel_next() {
    use host::rollup_core::RawRollupCore;
    use host::wasm_host::WasmHost;

    let msg = b"hello";
    WasmHost::write_debug(msg.as_ptr(), msg.len());
}

/// [`store_delete`] test kernel.
///
/// This kernel expects a collection of values to exist:
/// - `/durable/one`
/// - `/durable/one/two`
/// - `/durable/three`
/// - `/durable/three/four`
/// and deletes `/durable/one` & `/durable/three/four`.
///
/// [`store_delete`]: host::rollup_core::store_delete
#[cfg(feature = "test-store-delete")]
#[no_mangle]
pub unsafe extern "C" fn kernel_next() {
    use host::rollup_core::RawRollupCore;
    use host::wasm_host::WasmHost;

    let mut host = WasmHost::new();
    let host = &mut host;

    let path = b"/one";
    RawRollupCore::store_delete(host, path.as_ptr(), path.len());

    let path = b"/three/four";
    RawRollupCore::store_delete(host, path.as_ptr(), path.len());
}

/// [`store_list_size`] test kernel.
///
/// This kernel expects a collection of values to exist:
/// - `/durable/one`
/// - `/durable/one/two`
/// - `/durable/one/three`
/// - `/durable/one/four`
/// and checks that `store_list_size` returns `3`.
///
/// [`store_list_size`]: host::rollup_core::store_delete
#[cfg(feature = "test-store-list-size")]
#[no_mangle]
pub unsafe extern "C" fn kernel_next() {
    use host::rollup_core::RawRollupCore;
    use host::wasm_host::WasmHost;

    let mut host = WasmHost::new();
    let host = &mut host;

    let path = b"/one";
    let count = RawRollupCore::store_list_size(host, path.as_ptr(), path.len());
    assert_eq!(3, count);
}

/// [`store_move`] test kernel.
///
/// This kernel expects the following in durable storage:
/// - `/a/b`
/// - `/a/b/c`
/// - `/a/b/d`
/// - `/e/f`
/// and moves `/a/b` to location `/a/b/c`.
///
/// [`store_move`]: host::rollup_core::store_move
#[cfg(feature = "test-store-move")]
#[no_mangle]
pub unsafe extern "C" fn kernel_next() {
    use host::rollup_core::RawRollupCore;
    use host::wasm_host::WasmHost;

    let mut host = WasmHost::new();
    let host = &mut host;

    let from_path = b"/a/b";
    let to_path = b"/a/b/c";

    RawRollupCore::store_move(
        host,
        from_path.as_ptr(),
        from_path.len(),
        to_path.as_ptr(),
        to_path.len(),
    );
}

/// [`store_copy`] test kernel.
///
/// This kernel expects the following in durable storage:
/// - `/a/b`
/// - `/a/b/c`
/// - `/a/b/d`
/// - `/e/f`
/// and copies `/a/b` to location `/a/b/c`.
///
/// [`store_copy`]: host::rollup_core::store_copy
#[cfg(feature = "test-store-copy")]
#[no_mangle]
pub unsafe extern "C" fn kernel_next() {
    use host::rollup_core::RawRollupCore;
    use host::wasm_host::WasmHost;

    let mut host = WasmHost::new();
    let host = &mut host;

    let from_path = b"/a/b";
    let to_path = b"/a/b/c";

    RawRollupCore::store_copy(
        host,
        from_path.as_ptr(),
        from_path.len(),
        to_path.as_ptr(),
        to_path.len(),
    );
}
