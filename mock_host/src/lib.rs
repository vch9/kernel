//! Yielding rules implemented by [`mock_runtime`].

use host::rollup_core::Input;
use mock_runtime::{
    host::MockHost,
    state::{HostState, YieldStep},
};

/// The maximum number of attempts allowed to mark a level for input.
///
/// Used to prevent infinite loops when a user/test refuses to provide a level.
pub const MAX_ATTEMPTS_MARKING_NEXT_LEVEL: i32 = 10;

extern crate alloc;

/// Controls next step taken by host.
pub enum HostInput {
    /// Causes host to exit.
    Exit,
    /// Print current host state
    ShowState,
    /// Continue at next level
    NextLevel(i32),
}

/// Run a kernel, asking for input from the caller where required.
pub fn host_loop(
    init: HostState,
    kernel_next: impl Fn(&mut MockHost),
    host_next: impl Fn(i32) -> HostInput,
    get_input_batch: impl Fn(i32) -> Vec<(Input, Vec<u8>)>,
) -> HostState {
    let mut host: MockHost = init.into();

    'host: loop {
        match host.as_mut().handle_yield() {
            YieldStep::HandleYield => eprintln!("MOCK_HOST: handle yield"),
            YieldStep::Trampoline => kernel_next(&mut host),
            YieldStep::Reboot => {
                eprintln!("MOCK_HOST: reboot requested... exiting");
                break 'host;
            }
            YieldStep::MarkLevelForInput(level) => {
                let mut attempts = 0;
                'host_input: loop {
                    attempts += 1;
                    if attempts > MAX_ATTEMPTS_MARKING_NEXT_LEVEL {
                        eprintln!(
                            "MOCK_HOST: exhausted attempts asking for next input level"
                        );

                        break 'host;
                    }
                    eprintln!("MOCK_HOST: mark at least level {} for input", level);
                    match host_next(level) {
                        HostInput::Exit => {
                            eprintln!("MOCK_HOST: exiting at level {}", level);
                            break 'host;
                        }
                        HostInput::ShowState => println!("{:?}", &host),
                        HostInput::NextLevel(next) => {
                            eprintln!("MOCK_HOST: level {} marked for input", next);
                            host.as_mut().mark_level_for_input(next);
                            break 'host_input;
                        }
                    }
                }
            }
            YieldStep::InputTicks(level) => {
                let input = get_input_batch(level);
                host.as_mut().add_next_inputs(level, input.iter());
            }
        }
    }

    host.into_inner()
}
