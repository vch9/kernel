fn main() {
    main::r#impl();
}

#[cfg(not(target_arch = "wasm32"))]
mod main {
    #![allow(unused)]
    use debug::debug_msg;
    use host::rollup_core::Input;
    use mock_host::{host_loop, HostInput};
    use mock_runtime::{host::MockHost, state::HostState};

    extern crate alloc;

    /// Reads stdin and returns the command to be given to [`host_loop`].
    ///
    /// see [`HostInput`] for what the different options do.
    fn host_next(level: i32) -> HostInput {
        debug_msg!(
            MockHost,
            "Host input requested at {}: 'exit|state|<mark_level>'",
            level
        );

        let mut buffer = String::new();
        let stdin = std::io::stdin();
        stdin.read_line(&mut buffer).unwrap();
        match buffer.as_str() {
            "exit" => HostInput::Exit,
            "state" => HostInput::ShowState,
            l => HostInput::NextLevel(l.parse::<i32>().unwrap()),
        }
    }

    fn get_input_batch(level: i32) -> Vec<(Input, Vec<u8>)> {
        debug_msg!(MockHost, "input requested at level {}", level);
        vec![]
    }

    pub fn r#impl() {
        todo!("Allow ability to run kernels on cli")
    }
}

#[cfg(target_arch = "wasm32")]
mod main {
    pub fn r#impl() {
        panic!("mock_host is unsupported on wasm32");
    }
}
