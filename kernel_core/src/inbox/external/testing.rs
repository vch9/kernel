//! Arbitrary generation of [Signer], linked to a [BlsKey].
use proptest::prelude::*;

use crate::bls::BlsKey;

use super::Signer;

impl Signer {
    /// Generate an arbitrary `Signer`
    pub fn arb() -> BoxedStrategy<Signer> {
        (any::<bool>(), BlsKey::arb())
            .prop_map(|(is_address, key)| Signer::from((is_address, &key)))
            .boxed()
    }

    /// Generate an arbitrary `Signer`, together with its associated [`BlsKey`]
    pub fn arb_with_bls_key() -> BoxedStrategy<(Signer, BlsKey)> {
        (any::<bool>(), BlsKey::arb())
            .prop_map(|(is_address, key)| (Signer::from((is_address, &key)), key))
            .boxed()
    }
}

impl From<(bool, &BlsKey)> for Signer {
    fn from(from: (bool, &BlsKey)) -> Self {
        match from {
            (true, key) => Signer::Layer2Address(key.public_key_hash().clone()),
            (false, key) => Signer::BlsPublicKey(*key.compressed_public_key()),
        }
    }
}
