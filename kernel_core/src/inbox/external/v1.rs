//! V1 of external inbox message parsed into a kernel-specific data structure.
//!
//! Specifically, v1 allows a user to [transfer] & [withdraw].
//!
//! [transfer]: OperationTransfer
//! [withdraw]: OperationWithdraw
use std::iter::repeat;

use self::verifiable::VerificationError;

use super::Signer;
use crate::bls::{BlsError, CompressedSignature, PublicKey, Signature};
use crate::encoding::entrypoint::Entrypoint;
use crate::encoding::string_ticket::StringTicketRepr;
use crate::memory::{Account, Accounts};
use crypto::hash::{ContractKt1Hash, Layer2Tz4Hash};
use crypto::PublicKeyWithHash;
use host::rollup_core::RawRollupCore;
use nom::combinator::map;
use nom::multi::many1;
use nom::sequence::pair;
use tezos_encoding::enc::{BinError, BinWriter};
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::{dynamic, NomReader};
use thiserror::Error;
use verifiable::VerifiableTransaction;

pub mod sendable;
#[cfg(feature = "testing")]
pub mod testing;
pub mod verifiable;

/// Withdraws the ticket from the signer's account.
///
/// Ticket itself contains both the `creator`, `content` & `quantity`.
/// The withdrawal may be of quantity up to or equal the quantity of the
/// ticket that exists within the account.
///
/// The ticket may then be withdrawable on layer-1 by the account given by
/// `destination`.
#[derive(Debug, PartialEq, Eq, HasEncoding, NomReader, BinWriter)]
pub struct OperationWithdraw {
    destination: ContractKt1Hash,
    ticket: StringTicketRepr,
    entrypoint: Entrypoint,
}

/// Transfers the ticket from the signer's account, to the layer-2 `destination`.
///
/// Ticket itself contains both the `creator`, `content` & `quantity`.
/// The transfer may be of quantity up to or equal the quantity of the
/// ticket that exists within the account.
#[derive(Debug, PartialEq, Eq, HasEncoding, NomReader, BinWriter)]
pub struct OperationTransfer {
    destination: Layer2Tz4Hash,
    ticket: StringTicketRepr,
}

/// An operation either to withdraw or transfer a ticket.
#[derive(Debug, PartialEq, Eq, HasEncoding, NomReader, BinWriter)]
pub enum OperationContent {
    /// Withdraw a ticket from the signer account.
    Withdraw(OperationWithdraw),
    /// Transfer a ticket from the signer account to another Layer-2 account.
    Transfer(OperationTransfer),
}

impl OperationContent {
    /// Create a new withdrawal operation.
    pub fn withdrawal(
        destination: ContractKt1Hash,
        ticket: impl Into<StringTicketRepr>,
        entrypoint: Entrypoint,
    ) -> OperationContent {
        OperationContent::Withdraw(OperationWithdraw {
            destination,
            entrypoint,
            ticket: ticket.into(),
        })
    }

    /// Create a new transfer operation.
    pub fn transfer(
        destination: Layer2Tz4Hash,
        ticket: impl Into<StringTicketRepr>,
    ) -> OperationContent {
        OperationContent::Transfer(OperationTransfer {
            destination,
            ticket: ticket.into(),
        })
    }
}

/// A group of operations, operating on the account given by `signer`.
///
/// A signer may appear once per [VerifiableTransaction].
#[derive(Debug, PartialEq, Eq, HasEncoding, NomReader, BinWriter)]
pub struct Operation {
    /// The signer of this operation, who's account this will act against.
    pub signer: Signer,
    /// The counter of this operation - must match the signer's current counter.
    pub counter: i64,
    /// The individual actions contained within this operation.
    pub contents: Vec<OperationContent>,
}

/// Errors occurring when signing or serializing operations, transactions and batches.
#[derive(Debug, Error)]
pub enum ToBytesError {
    /// Bls signing/aggregation error
    #[error("Bls error in signatures {0}")]
    Bls(#[from] BlsError),
    /// Serialization error
    #[error("Unable to serialize operation: {0}")]
    Binary(#[from] BinError),
    /// Incorrect signing key
    #[error("BlsKey did not match Signer")]
    IncorrectKey,
}

/// A batch of operations, associated with an aggregated signature.
#[derive(Debug, PartialEq, Eq)]
pub struct ParsedBatch<'a> {
    /// List of transactions, to be applied in order.
    pub transactions: Vec<VerifiableTransaction<'a>>,
    /// Compressed aggregated signature, used to verify signatures on all transactions.
    pub aggregated_signature: CompressedSignature,
}

impl<'a> ParsedBatch<'a> {
    /// Parse a batch, where each transaction is *verifiable*.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        map(
            pair(
                dynamic(many1(VerifiableTransaction::parse)),
                CompressedSignature::nom_read,
            ),
            |(transactions, aggregated_signature)| ParsedBatch {
                transactions,
                aggregated_signature,
            },
        )(input)
    }

    /// Verify signature of the batch.
    pub fn verify_signature<Host: RawRollupCore>(
        &self,
        accounts: &mut Accounts,
    ) -> Result<(), VerificationError> {
        let sig = Signature::try_from(&self.aggregated_signature)?;

        let mut messages = Vec::new();

        for (encoded, signer) in self
            .transactions
            .iter()
            .flat_map(|t| repeat(t.encoded()).zip(t.signers()))
        {
            let pk = match signer {
                // Link the public key with the account, if it hasn't been already.
                Signer::BlsPublicKey(key) => {
                    let pk = PublicKey::try_from(key)?;
                    let address = key.pk_hash()?;

                    if let Some(account) = accounts.account_of_mut(&address) {
                        if account.public_key().is_none() {
                            account.link_public_key(pk.clone());
                        }
                    } else {
                        let mut account = Account::default();
                        account.link_public_key(pk.clone());
                        accounts.add_account(address, account)?;
                    }

                    pk
                }
                // Lookup the public key from the account with the given address
                Signer::Layer2Address(address) => accounts
                    .account_of(address)
                    .ok_or_else(|| VerificationError::NoAccountOfAddress(address.clone()))
                    .map(|account| {
                        account
                            .public_key()
                            .ok_or_else(|| {
                                VerificationError::NoPublicKeyForAddress(address.clone())
                            })
                            .map(|pk| pk.clone())
                    })??,
            };

            messages.push((encoded, pk));
        }

        let mut messages = messages.iter().map(|(encoded, pk)| (*encoded, pk));

        // Signature verification consumes many ticks (on the order of 2Billion)
        // Therefore provide an optional, non-default, feature to disable verif
        // for use in tests.
        #[cfg(not(feature = "tx-kernel-no-sig-verif"))]
        if sig.aggregate_verify(&mut messages)? {
            Ok(())
        } else {
            Err(VerificationError::SignatureVerificationError)
        }
        #[cfg(feature = "tx-kernel-no-sig-verif")]
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;
    use tezos_encoding::nom::NomReader;

    use super::OperationContent;

    proptest! {
        #[test]
        fn operation_withdraw_encode_decode(
            operation in OperationContent::arb_withdrawal(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            let mut encoded = Vec::new();
            operation
                .bin_write(&mut encoded)
                .expect("encoding withdraw operation should work");
            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, decoded) = OperationContent::nom_read(encoded.as_slice())
                .expect("decoding withdraw operation should work");

            assert_eq!(remaining, remaining_input.as_slice());

            assert_eq!(operation, decoded);
        }

        #[test]
        fn operation_transfer_encode_decode(
            operation in OperationContent::arb_transfer(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            let mut encoded = Vec::new();
            operation
                .bin_write(&mut encoded)
                .expect("encoding transfer operation should work");
            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, decoded) = OperationContent::nom_read(encoded.as_slice())
                .expect("decoding transfer operation should work");

            assert_eq!(remaining, remaining_input.as_slice());

            assert_eq!(operation, decoded);
        }
    }
}
