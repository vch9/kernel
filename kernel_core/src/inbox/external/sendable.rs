//! Constructing external inbox messages for sending to the kernel.

use tezos_encoding_derive::{BinWriter, HasEncoding};

use super::v1;

/// Upgradeable representation of external inbox messages.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum ExternalInboxMessage {
    /// Version 1 of operation batching
    V1(v1::sendable::Batch),
}
