//! Processing external inbox messages - withdrawals & transactions.

use crate::inbox::v1::verifiable::VerifiedTransaction;
use crate::inbox::ExternalInboxMessage;
use crate::inbox::ParsedExternalInboxMessage;
use crate::memory::Accounts;
use debug::debug_msg;
use host::rollup_core::RawRollupCore;

/// Possible outcomes when processing messages
pub enum ProcessedOutcome {
    /// A verified transaction
    Transaction(VerifiedTransaction),
    /// Call again for the next transaction
    More,
    /// Finished processing transactions
    Finished,
}

/// Prepare an external message for processing, in a stepwise manner.
///
/// Returns a closure, that may be called to perform the next step of
/// processing an external message.
pub fn prepare_for_processing<'a, Host>(
    message: ExternalInboxMessage<'a>,
    accounts: &mut Accounts,
) -> Option<impl FnMut(&Accounts) -> ProcessedOutcome + 'a>
where
    Host: RawRollupCore,
{
    let ParsedExternalInboxMessage::V1(batch) = parse_external::<Host>(message)?;

    if let Err(e) = batch.verify_signature::<Host>(accounts) {
        debug_msg!(Host, "Signature verification of batch failed: {}", e);
        return None;
    }

    let mut transactions = batch.transactions.into_iter();

    let step_fn = move |accounts: &Accounts| {
        let outcome = transactions.next().map(|t| t.verify(accounts));

        match outcome {
            None => ProcessedOutcome::Finished,
            Some(Ok(verified_transaction)) => {
                ProcessedOutcome::Transaction(verified_transaction)
            }
            Some(Err(err)) => {
                debug_msg!(Host, "Could not verify transaction: {}", err);

                ProcessedOutcome::More
            }
        }
    };

    Some(step_fn)
}

/// Parse external message, logging error if it occurs.
fn parse_external<Host: RawRollupCore>(
    message: ExternalInboxMessage,
) -> Option<ParsedExternalInboxMessage> {
    match ParsedExternalInboxMessage::parse(message.0) {
        Ok((remaining, external)) => {
            if !remaining.is_empty() {
                debug_msg!(
                    Host,
                    "External message had unused remaining bytes: {:?}",
                    remaining
                );
            }
            Some(external)
        }
        Err(err) => {
            debug_msg!(Host, "Error parsing external message payload {}", err);
            None
        }
    }
}
