//! Deposit tickets into the kernel state.

use crypto::hash::Layer2Tz4Hash;
use debug::debug_msg;
use host::rollup_core::RawRollupCore;
use num_bigint::TryFromBigIntError;
use thiserror::Error;

use crate::{
    encoding::string_ticket::{StringTicket, TicketHashError},
    memory::{Account, AccountError, Memory},
};

/// Errors that may occur when depositing a ticket into an account.
#[derive(Error, Debug)]
pub enum DepositError {
    /// Issue occurred while handling depositee account.
    #[error("{0}")]
    AccountError(#[from] AccountError),

    /// Issue occurred hashing ticket.
    #[error("Error hashing ticket contents: {0}")]
    TicketHash(#[from] TicketHashError),

    /// Deposited ticket quantity too large.
    #[error("Ticket amount too large to deposit: {0}")]
    TicketAmount(#[from] TryFromBigIntError<()>),
}

/// Deposits ticket into account.
///
/// Returns error if amount is negative, or if the amount is greater than can fit
/// in the account.
pub fn deposit_ticket<Host: RawRollupCore>(
    memory: &mut Memory,
    account_address: Layer2Tz4Hash,
    ticket: StringTicket,
) -> Result<(), DepositError> {
    let ticket_amount = ticket.amount();
    let id_proof = ticket.identify_trustless()?;

    debug_msg!(
        Host,
        "Depositing {:#?} with identity {:?} into account {:?}",
        id_proof.ticket(),
        id_proof.identity(),
        &account_address
    );

    let result: Result<Option<Account>, AccountError> = memory
        .accounts_mut()
        .account_of_mut(&account_address)
        .map_or_else(
            || {
                // No previous account at address
                let mut account = Account::default();
                account.add_ticket(id_proof.identity().clone(), ticket_amount)?;
                Ok(Some(account))
            },
            |account| {
                account.add_ticket(id_proof.identity().clone(), ticket_amount)?;
                Ok(None)
            },
        );

    match result? {
        Some(new_account) => memory
            .accounts_mut()
            .add_account(account_address, new_account)?,
        None => (),
    }

    // Update global ticket table
    memory.add_ticket(id_proof);

    Ok(())
}

#[cfg(test)]
mod test {
    use crypto::hash::HashTrait;
    use mock_runtime::host::MockHost;
    use num_bigint::BigInt;

    use crate::encoding::contract::Contract;

    use super::*;

    #[test]
    fn deposit_ticket_into_account() {
        // Arrange
        let mut memory = Memory::default();

        let destination =
            Layer2Tz4Hash::from_b58check("tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K").unwrap();

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();

        let ticket_content = "hello, ticket!";
        let ticket_quantity = BigInt::from(5_i32);

        let ticket = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity.try_into().unwrap(),
        );

        let identity = ticket.identify().unwrap();

        // Act
        assert!(!memory.ticket_recognized(&identity));

        let result = deposit_ticket::<MockHost>(&mut memory, destination.clone(), ticket);

        // Assert
        assert!(result.is_ok());
        assert!(memory.ticket_recognized(&identity));

        let account = memory
            .accounts()
            .account_of(&destination)
            .cloned()
            .expect("account should be created");

        let mut expected = Account::default();
        expected.add_ticket(identity, 5).unwrap();

        assert_eq!(expected, account);
    }

    #[test]
    fn deposit_multiple_tickets_into_account() {
        // Arrange
        let mut memory = Memory::default();

        let destination =
            Layer2Tz4Hash::from_b58check("tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K").unwrap();

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();

        let make_first = |amount: i32| {
            StringTicket::new(
                ticket_creator.clone(),
                "hello, ticket".to_string(),
                amount.try_into().unwrap(),
            )
        };
        let make_second = |amount: u64| {
            StringTicket::new(ticket_creator.clone(), "hello, world".to_string(), amount)
        };

        let first_identity = make_first(1).identify().unwrap();
        let second_identity = make_second(1).identify().unwrap();

        deposit_ticket::<MockHost>(&mut memory, destination.clone(), make_first(5))
            .unwrap();

        // Act
        deposit_ticket::<MockHost>(&mut memory, destination.clone(), make_first(15))
            .unwrap();
        deposit_ticket::<MockHost>(&mut memory, destination.clone(), make_second(121))
            .unwrap();

        // Assert
        assert!(memory.ticket_recognized(&first_identity));
        assert!(memory.ticket_recognized(&second_identity));

        let account = memory
            .accounts()
            .account_of(&destination)
            .cloned()
            .expect("account should be created");

        let mut expected = Account::default();
        expected.add_ticket(first_identity, 20).unwrap();
        expected.add_ticket(second_identity, 121).unwrap();

        assert_eq!(expected, account);
    }
}
